package br.ucsal.testequalidade.bes20181.calculo;

import java.util.Scanner;

public class Exemplo1 {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		Exemplo1 exemplo1 = new Exemplo1();

		System.out.println("Informe o valor:");
		int n = scanner.nextInt();
		exemplo1.calcularPrimo(n);
		exemplo1.calcularFatorial(n);

	}

	private void calcularPrimo(int n) {
		long qtdDiv = 0;

		for (int i = 2; i <= n / 2; i++) {
			if (n % i == 0) {
				qtdDiv++;
			}
		}

		if (qtdDiv == 0) {
			System.out.println(n + " � primo.");
		} else {
			System.out.println(n + " n�o � primo.");
		}
	}

	private void calcularFatorial(int n) {
		long fat = 1;

		for (int i = 1; i <= n; i++) {
			fat *= i;
		}
		System.out.println("Fatorial(" + n + ")=" + fat);
	}

}
