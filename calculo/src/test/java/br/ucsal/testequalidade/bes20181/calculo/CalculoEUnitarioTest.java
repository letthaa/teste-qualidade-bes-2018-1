package br.ucsal.testequalidade.bes20181.calculo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class CalculoEUnitarioTest {

	CalculoE calculoE;

	Fatorial fatorialMock;

	@Before
	public void setup() {
		// Constru��o de infra-estrutura para o teste (configura��o, setup)
		fatorialMock = Mockito.mock(Fatorial.class);
		calculoE = new CalculoE(fatorialMock);
	}

	@Test
	public void calculoE2() {
		// Defini��o de comportamento para objetos mocados
		Mockito.when(fatorialMock.calcularFatorial(0)).thenReturn(1L);
		Mockito.when(fatorialMock.calcularFatorial(1)).thenReturn(1L);
		Mockito.when(fatorialMock.calcularFatorial(2)).thenReturn(2L);
		
		// Dados de entrada
		Integer n = 2;

		// Sa�da esperada
		Double eEsperado = 2.5d;

		// Execu��o do m�todo em teste e obten��o do resultado atual
		Double eAtual = calculoE.calcularE(n);

		// Comparar o resultado esperado com o resultado atual. 
		Assert.assertEquals(eEsperado, eAtual, 0.001);
		
		// Verifica��o do uso dos objetos mocados
		Mockito.verify(fatorialMock).calcularFatorial(0);
		Mockito.verify(fatorialMock).calcularFatorial(1);
		Mockito.verify(fatorialMock).calcularFatorial(2);
	}

}
