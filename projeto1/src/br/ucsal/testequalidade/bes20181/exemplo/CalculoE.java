package br.ucsal.testequalidade.bes20181.exemplo;

public class CalculoE {

	Fatorial fatorial;

	CalculoE(Fatorial fatorial) {
		this.fatorial = fatorial;
	}

	Double calcularE(Integer n) {
		Double e = 0d;
		for (int i = 0; i <= n; i++) {
			e += 1. / fatorial.calcularFatorial(i);
		}
		return e;
	}

}
